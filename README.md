# Italian Modules Language Pack

One translation module to rule them all!  
The aim of this module is to include all existing Italian translations of Foundry modules in a single module.  
This allows for simplified, unified and quick management in response to frequent updates or creation of new modules.

Below is the list of currently translated modules:

* About Time
* Active Tile Triggers
* Argon Combat HUD
* Arms Reach
* Better Roofs
* Curvy Walls
* Drag Ruler
* DAE
* FXMaster
* Hero Creation Tool
* Levels
* Midi QOL
* Monk's Token Bar
* Quick Encounters
* Simple Calendar
* Splatter
* Tidy Sheet
* Token Action HUD
* Token Magic FX
* Wall Height
* Weather Blocker
* Weather Control

Anyone who wants to contribute by suggesting new modules to add to the list or to report errors/improvements or even help in the translation, please subscribe to the discord [Foundry_ITA](https://discord.gg/GKeuEq3J) server and post your question/membership in the [fvtt-translation-modules](https://discord.gg/caBAtZp7) channel, thanks!
